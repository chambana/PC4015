import math

#Parse the string and return applicable data needed
#Input as a string '$GPGGA,123519,4807.0380,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47'
#Output as a string ['123519','4807.0380','N','01131.0000','E',1,08]
#Output as follows [UTC time, Lat, N or S, Long, E or W, GPS Qual, # of sat]
def createGPGGA(x):
    #Split input into a matrix with 15 elements (based on input string
    list1=x.split(',')
    coorddata = []
    coorddata.append(list1[1])
    coorddata.append(list1[2])
    coorddata.append(list1[3])
    coorddata.append(list1[4])
    coorddata.append(list1[5])
    coorddata.append(float(list1[6]))
    coorddata.append(int(list1[7]))
    return coorddata

#Take a latitude/longitude input and create a degree decimal
#Input (deg string, Dir)
#Output (degree decimal)
def degsec(x,y):
    deg = float(x[0:2]) + float(x[2:9])/60
    if y == 'S' or y == 'W':
        deg = -deg
    return deg

#Take a latitude/longitude inut and create a minute decimal
#Input (deg string, Dir)
#Output (min decimal)
def minsec(x,y):
    minute =float(x[2:9])
    if y == 'S' or y == 'W':
        minute = -minute
    return minute

test = '$GPGGA,123519,4807.0380,S,0113.0000,E,1,08,0.9,545.4,M,46.9,M,,*47'
test2 = createGPGGA(test)
print (test2)
latdeg = degsec(test2[1],test2[2])
print (latdeg)
latmin = minsec(test2[1],test2[2])
print(latmin)

