from collections import namedtuple

Waypoint = namedtuple("Waypoint", ["id", "lat", "lon"])

test_path = [(36.595306,-121.875168),(36.595112,-121.874945)]
#test_path = [(36.595306,-121.875168),(36.595393,-121.874979),(36.595112,-121.874945)]

#test_path = [("36:35:42.40N","121:52:29.80W"),("36:35:43.10N","121:52:30.60W"),("36:35:43.41N","121:52:29.92W")]
def _setWP_ID(self):
    '''Set Waypoint Identifier name'''
    ('Waypoint', ('id',self))
@property
def WP_ID(self):
    return self.id
def WP_lat(self):
    return self.lat
def WP_lon(self):
    return self.lon
def __str__(self):
    return 'Waypoint: id=%s lat=%s lon=%s'% (self.id, self.lat, self.lon)
#Create Waypoint instance
wp = Waypoint(1,36.595306,-121.875168)

#display
print(wp)
print("Waypoint details: ID:{}\t Lat:{}\t Lon:{}\t".format(wp.id,wp.lat,wp.lon ))
print(wp.__str__())
print(wp.__doc__)
print(type(wp.__repr__()))


class ExampleClassName(object):

    def __init__(self, some_input_variable):

        self.private_variable = some_input_variable
        print "class init function got variable: ", self.private_variable

    def some_function(self):
        print "sample output"





if __name__ == "__main__":

    my_object = ExampleClassName(4)
    print "made an object", my_object
