from AriaPy import *
import sys
import time


# Action that drives the robot forward, but stops if obstacles are
# detected by sonar.
class ActionGo(ArAction):

    # constructor, sets myMaxSpeed and myStopDistance
    def __init__(self, maxSpeed, stopDistance):
        ArAction.__init__(self, "Go")
        self.myMaxSpeed = maxSpeed
        self.myStopDistance = stopDistance
        self.myDesired = ArActionDesired()
        self.mySonar = None
        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("maximum speed", self.myMaxSpeed, "Maximum speed to go."))
        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("stop distance", self.myStopDistance, "Distance at which to stop."))

    # This fire method is where the real work of the action happens.
    # currentDesired is the combined desired action from other actions
    # previously processed by the action resolver.  In this case, we're
    # not interested in that, we will set our desired
    # forward velocity in the myDesired member, and return it.
    # Note that myDesired must be a class member:, since this method
    # will return a pointer to myDesired to the caller. If we had
    # declared the desired action as a local variable in this method,
    # the pointer we returned would be invalid after this method
    # returned.
    def fire(self, currentDesired):
        # reset the actionDesired (must be done), to clear
        # its previous values.
        self.myDesired.reset()

        # if the sonar is null we can't do anything, so deactivate
        if self.mySonar == None:
            self.deactivate()
            return None

        # get the range of the sonar
        range = self.mySonar.currentReadingPolar(-90, 90) - self.getRobot().getRobotRadius()

        time.sleep(.1)
        range_mod_right = self.mySonar.currentReadingPolar(-120, -70)
        time.sleep(.1)
        range_mod_left = self.mySonar.currentReadingPolar(70, 120)
        print('Sonar range Right: ', range_mod_right)
        print('Sonar range left: ', range_mod_left)
        # if the range is greater than the stop distance, find some speed to go
        if (range > self.myStopDistance):
            # just an arbitrary speed based on the range
            speed = range * .3
            # if that speed is greater than our max, cap it
            if (speed > self.myMaxSpeed):
                speed = self.myMaxSpeed
            # now set the velocity
            self.myDesired.setVel(speed)
        else:
            # the range was less than the stop distance, so request stop
            self.myDesired.setVel(0)

        # return a reference to our actionDesired to the resolver to make our request
        return self.myDesired


    # Override setRobot() to get a reference to the sonar device
    def setRobot(self, robot):

        # Set myRobot object in parent ArAction class (must be done if
        # you overload setRobot):
        # self.myRobot = robot
        print "ActionGo: setting robot on ArAction..."
        self.setActionRobot(robot)

        # Find sonar device for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if (self.mySonar == None):
            ArLog.log(ArLog.Terse, "actionExample: ActionGo: Warning: The robot had no sonar range device, deactivating!")
            self.deactivate()


# Action that turns the robot away from obstacles detected by the
# sonar.

class ActionTurn(ArAction):
    def __init__(self, target_range, turnAmount, deadband_width):
        ArAction.__init__(self, "Turn")
        self.myDesired = ArActionDesired()
        self.target_range = target_range
        self.myTurnAmount = turnAmount
        self.deadband_width = deadband_width

        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("turn threshold (mm)", self.myTurnThreshold, "The number of mm away from obstacle to begin turnning."))
        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("turn amount (deg)", self.myTurnAmount, "The number of degress to turn if turning."))

        # remember which turn direction we requested, to help keep turns smooth
        self.myTurning = 0 # -1 == left, 1 == right, 0 == none


    def setRobot(self, robot):
        # Sets myRobot in the parent ArAction class (must be done):
        print "ActionTurn: calling ArAction.setActionRobot..."
        self.setActionRobot(robot)
        # self.myRobot = robot


        # Find sonar object for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if (self.mySonar == None):
            ArLog.log(ArLog.Terse, "actionExample: ActionTurn: Warning: I found no sonar, deactivating.")
            self.deactivate()

    def fire(self, currentDesired):

        # reset the actionDesired (must be done)
        self.myDesired.reset()

        # if the sonar is null we can't do anything, so deactivate
        if self.mySonar == None:
            self.deactivate()
            return None

        # Get the left readings and right readings off of the sonar
        #leftRange = (self.mySonar.currentReadingPolar(0, 100) -
        #             self.getRobot().getRobotRadius())
        rightRange = (self.mySonar.currentReadingPolar(-100, 0) -
                      self.getRobot().getRobotRadius())

        print "Current sonar reading is: ", rightRange, "mm"

        # if neither left nor right range is within the turn threshold,
        # reset the turning variable and don't turn
        #if (leftRange > self.myTurnThreshold  and  rightRange > self.myTurnThreshold):
        if (self.target_range - self.deadband_width) < rightRange < (self.target_range + self.deadband_width):
            print "Inside Deadband, don't turn"
            self.myTurning = 0
            self.myDesired.setDeltaHeading(0)

        # OK, we're not in the target zone, but are we already turning?
        # if we're already turning some direction, keep turning that direction
        elif (self.myTurning != 0):
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)

        # OK, we're not turning, but need to...
        #turn left
        elif (rightRange < self.target_range):
            print "Turning left:  delta heading is", self.myTurnAmount * self.myTurning
            self.myTurning = 1
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)

        #turn right
        else :
            print "Turning right:  delta heading is", self.myTurnAmount * self.myTurning
            self.myTurning = -1
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)

        # return the actionDesired, so resolver knows what to do
        return self.myDesired

def setupRun(self):
    try:
        Aria_init()
        parser = ArArgumentParser(sys.argv)
        parser.loadDefaultArguments()
        robot = ArRobot()
        conn = ArRobotConnector(parser, robot)
        sonar = ArSonarDevice()
        keyHandler = Aria_setKeyHandler(self)
        robot.attachKeyHandler(self,keyHandler)
        # Connect to the robot
        if not conn.connectRobot():
            ArLog.log(ArLog.Terse, "Unable to connect to robot. Exiting.")
            Aria_exit(1)
        # Parse all command-line arguments
        if not Aria_parseArgs():
            Aria_logOptions()
            Aria_exit(1)
    except Exception as e:
        print(e, 'setupRun() failed. Exiting...')
        #prompt user if teleop desired continue otherwise exit
        Aria_exit(0)
    else:
        configureWallFollow(self,robot)
        robot.run(1)
    # Run the robot processing cycle.
    # if'true' means to return if it loses connection, then exit
    finally:
        Aria_exit(0)

# Add our actions in order. The second argument is the priority,
# with higher priority actions going first, and possibly pre-empting lower
# priority actions.

def setpriority(self,val):
    return val
def setRunparam(): pass

default_run_params={'maxSpeed':500,'stopDistance':400,'target_range':400,'turnAmount':0,'deadband_width':30,'lowpriority':[100,50,40]}



def configureWallFollow(self,robot):
    # Add the range device to the robot before adding actions.
    robot.addRangeDevice(self.sonar)

    go = ActionGo(500, 350)
    turn = ActionTurn(target_range=400, turnAmount=10, deadband_width=30)
    recover = ArActionStallRecover()

    robot.addAction(recover, setpriority=100)
    robot.addAction(go, setpriority=50)
    robot.addAction(turn, setpriority=49)

    # Enable the motors
    robot.enableMotors()




