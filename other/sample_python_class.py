

class ExampleClassName(object):

    def __init__(self, some_input_variable):

        self.private_variable = some_input_variable
        print "class init function got variable: ", self.private_variable

    def some_function(self):
        print "sample output"





if __name__ == "__main__":

    my_object = ExampleClassName(4)
    print "made an object", my_object
