import math
import os
import time
import sys


#raw_input = input('Enter wpts? y/n \n')


def main():
	#init condition variable to loop forever until break reached
	usrchoice = "0"
						
	print('*********** MENU ***********')
	print('\t')
	print('1. Start gotowaypoint mode')
	print('2. Start teleop mode')
	print('3. Add waypoint mode')
	print('\t')
	valid_entry_li=['1','2','3','q'] #list of accepted values
	
	
	#if userinput from rawinput is not in list accepted keys
	#continue or exit
	runLoop = True
	while(runLoop):
		
		usrchoice = raw_input("Enter choice or press q to quit \n")
		try:
			print('attempting to execute user selection \n')
			if usrchoice in valid_entry_li:
				if usrchoice == "1":
					print("Selected: DriveToWaypoint mode")
					#TODO 
					break
				elif usrchoice == "2":
					print("Selected: Teleop mode")
					#TODO 
					break
				elif usrchoice == "3":
					print("Add waypoints chosen")
					#TODO 
					break
				elif usrchoice =="q":
					print('Exiting. Please wait')
					time.sleep(2)
					return(0)
				runLoop=False	#met condition, break from while loop
				break
			else:
				print("Invalid input. Try again, or press q to quit \n")
		except Exception as e:
			print(e) 
			print("Fail to complete usrchoice {}".format(usrchoice))
			 	
			
		
			
				

				
		#else:
		#no problems, check if usrchoice is member of accepted inputs
	
	
	return 0

if __name__ == '__main__':
	main()
