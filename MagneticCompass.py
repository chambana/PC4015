"""
The serial read to the compass provides an NMEA string:
$PTNPHTR,HHH.H,N,P.P,N,R.R,N*hh.
HHH.H = Heading information in degrees
P.P = Pitch information in degrees
R.R = roll information in degrees
N = Heading, Pitch, and Roll status
*hh is for checksum parity
<CR> = Carriage Return
<LF>= Line Feed
"""

import serial
from time import sleep
import time
import threading
import Queue

class MagneticCompass(object):

    #initialize Magnetic Compass Object that will be used for monitoring the magnetic compass
    def __init__(self, var, vardir,port="/dev/ttyS1"):
        ''' Base obj , attri'''
        self.latest_raw_heading=None

        self.ser = serial.Serial(port, 19200, timeout=0)
        trash=self.ser.readlines()
        self.variation = var

        self.compass_queue = Queue.LifoQueue()
        self._stopevent = threading.Event()

        self.thread2 = threading.Thread(target=self.infinite_loop_compass_grabber, args=(self.compass_queue,))
        self.thread2.start()
        self.variationdir = vardir
        self.variation = var



    def getcompassdata(self):
        ''' '''
        if self.compass_queue.qsize()!=0:
            self.latest_raw_heading= self.compass_queue.get()
        else:
            pass
#            print "no new data since last update, using last measurement"
        return self.latest_raw_heading


    def infinite_loop_compass_grabber(self, data_transfer_queue):
        print "Started infinite loop for handling COMPASS hardware in separate thread"
        while not self._stopevent.isSet():
            try:
                #print "thread 2 is grabbing"
                raw_heading = self.__get_raw_compass_sentence()
                if raw_heading!=None:
                    data_transfer_queue.put(raw_heading)
                #print "***thread 2 got", raw_heading
                #print "queue size:", data_transfer_queue.qsize()
                #time.sleep(0.1)
            except Exception as e:
                print "Exception in hardware handling thread", str(e)
                #self.ser.close()
                #quit(1)
                continue


    def __get_raw_compass_sentence(self):
        while 1:

            time.sleep(.05)
            x = self.ser.readline()
            y= self.ser.inWaiting()
            temp=self.ser.read(y)

            if not x or len(x) <= 14:
                return None
                # continue

            if x[0] != '$':
                # continue
                return None
            else:
                # we have a start character
                temp = x[0:14]
                # print "sliced out", temp
                sliced = temp.split(',')
                heading = sliced[1]
                #print "__get_raw_compass got", heading
                try:
                    heading_float = float(heading)
                    return float(heading)
                except:
                    return None

    #Input dictionary, variation (float), and variation direction (E/W) to get true heading
    def trueheading(self, heading_mag_float):
        #magneticheading = magneticdict.get('Heading')
        if self.variationdir == 'E':
            trueheadingvar = float(heading_mag_float) + self.variation
            if trueheadingvar<0:
                trueheadingvar = 360 + trueheadingvar
            #print "DEBUG, got", heading_mag_float, "sending", trueheadingvar, self.variation
            return trueheadingvar
        else:
            trueheadingvar = float(heading_mag_float) + self.variation
            return trueheadingvar


if __name__ =='__main__':
    #Code testing
    compass = MagneticCompass(var=13.28,vardir='E')
    print compass
    while 1:
        try:
            #print "Trying to read compass"
            raw_heading = compass.getcompassdata()
            #print "Main Thread got Raw Heading", raw_heading
            if raw_heading!=None:
                trueheading = compass.trueheading(raw_heading)
                print "Main Thread calc'd True Heading: ",trueheading
            time.sleep(0.05)

        except KeyboardInterrupt as e:
            print "got keyboard quit", str(e)
            compass._stopevent.set()
            break

        except Exception as e:
            print "asking thread2 to die", str(e)
            compass._stopevent.set()
            break
    compass.ser.close()
