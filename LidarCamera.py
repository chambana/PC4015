import numpy as np
from math import *
import serial
from hokuyo.driver import hokuyo
from hokuyo.tools import serial_port
import pygame
import math
from camera import *


#Guaranteed Range: 0.1 ~ 30,000mm (White Kent Sheet)

MAX_RANGE_TO_DISPLAY = 1000 #in mm
MAX_RANGE = 30000
WINDOW_WIDTH = 1920
WINDOW_HEIGHT = 1080
pixel_per_mm_ratio = (1024.0/2)/MAX_RANGE_TO_DISPLAY
X=0
Y=1
pixel_delta_threshold_for_drawing_distance = 100 #in pixels

class Lidar(object):

    def __init__(self, lidar_position_on_robot_in_mm=254, uart_port = '/dev/ttyACM0', uart_speed = 19200):
        self.lidar_offset_x = lidar_position_on_robot_in_mm  #LIDAR on the Pioneer is located 254mm +X from centerpoint
        laser_serial = serial.Serial(port=uart_port, baudrate=uart_speed, timeout=0.5)
        port = serial_port.SerialPort(laser_serial)
        self.laser = hokuyo.Hokuyo(port)
        self.laser.laser_on()

    def __create_homogeneous_transform_matrix(self, alpha_deg, quadrant1_has_positive_angles=False ):
        #TODO:  if this is too slow, make a static array of the transform matrices for each angle

        if quadrant1_has_positive_angles==False:
            alpha_deg = -alpha_deg
        angle_rad = radians(alpha_deg)
        #this matrix will project points from LIDAR frame to the Robot's frame
        Homogeneous_Transform_R_L = np.matrix([[cos(angle_rad), -sin(angle_rad), 0, self.lidar_offset_x],
                                               [sin(angle_rad), cos(angle_rad),  0,        0           ],
                                               [0,                   0,          1,        0           ],
                                               [0,                   0,          0,        1           ]])

        return Homogeneous_Transform_R_L

    def __get_point_cloud_raw(self):
        one_scan = self.laser.get_single_scan()
        for item in sorted(one_scan):
        #     if one_scan[item]<10:
        #         print "DEBUG", item, one_scan[item]
            if one_scan[item]==1 or one_scan==0:
                one_scan[item]=MAX_RANGE
        return one_scan

    #this function gets a raw point cloud and transforms it via homoegeneous transform matrix
    def get_point_cloud(self):
        transformed_points_list = []
        raw_cloud = self.__get_point_cloud_raw()
        for angle_deg, distance in sorted(raw_cloud.iteritems()):
            homogeneous_transform_Robot_from_Lidar = self.__create_homogeneous_transform_matrix(angle_deg)
            point_in_frame_Lidar = np.transpose(np.matrix([distance, 0, 0, 1]))
            point_in_frame_Robot = np.dot(homogeneous_transform_Robot_from_Lidar, point_in_frame_Lidar)
            transformed_points_list.append(point_in_frame_Robot)
            #print "Angle", angle_deg, "dist", distance
        return transformed_points_list


    #CCW calculation
    def currentReadingPolar(self, start_angle_deg, stop_angle_deg, using_negative_angles_in_quadrant_1 = True):

        #pioneer format
        #leftRange = self.mySonar.currentReadingPolar(70,110)
        #rightRange = self.mySonar.currentReadingPolar(-110, -70)
        #hokuyo format
        # leftRange = self.mySonar.currentReadingPolar(-70, -110)
        # rightRange = self.mySonar.currentReadingPolar(110, 70)

        if using_negative_angles_in_quadrant_1==False:
            start_angle_deg=-start_angle_deg
            stop_angle_deg=-stop_angle_deg

        raw_ranges = self.__get_point_cloud_raw()
        closest_range = 30000 #mm

        for angle in raw_ranges:
            if start_angle_deg<=angle<=stop_angle_deg:
                if raw_ranges[angle] < closest_range:
                    closest_range=raw_ranges[angle]

        return closest_range

    def shutdown(self):
        print "Shutting Down Laser"
        self.laser.laser_off()


def text_objects(text, font):
    textSurface = font.render(text, True, (0,0,0))
    return textSurface, textSurface.get_rect()

if __name__ == "__main__":
    print "Testing LIDAR class..."

    my_laser = Lidar(lidar_position_on_robot_in_mm=254)
    my_laser.get_point_cloud()

    filename = "pioneer_top.jpg"

    #Pioneer is ~20inches by ~20 inches = ~550mm by ~550mm
    #calculate how many pixels the rover should be displayed as -- based on its true dimensions
    rover_image_height = int(550*pixel_per_mm_ratio)
    rover_image_width = int(550*pixel_per_mm_ratio)

    img = pygame.image.load(filename)
    img = pygame.transform.scale(img,(rover_image_width, rover_image_height))

    print("loaded", img)
    screen = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
    clock = pygame.time.Clock()
    running = True

    rover_center = (WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2)

    pygame.font.init()
    largeText = pygame.font.SysFont("monospace", 25, bold=True)



    myCam = Capture()
    #myCam.main()

    while running:

        transformed_points = my_laser.get_point_cloud()

        screen.fill((155, 155, 155))
        screen.blit(img, ((WINDOW_WIDTH / 2) - (0.5 * rover_image_width), (WINDOW_HEIGHT / 2) - (0.5 * rover_image_height)))

        myCam.get_and_flip()


        #print transformed_points



        for index, single_point in enumerate(transformed_points):

            point_in_frame_coords=myCam.projection(point_ground_x=single_point[0], point_ground_y=single_point[1])

            #print "index", index, "single point", single_point

            #pygame.draw.lines(screen, (0,255,0), False, [(rover_center[X],rover_center[Y]-(my_laser.lidar_offset_x*pixel_per_mm_ratio)), (rover_center[X] + int(-single_point[Y]*pixel_per_mm_ratio), rover_center[Y] + int(-single_point[X]*pixel_per_mm_ratio))], 2)
            try:
                print "bottom origin to endpoint: ",((WINDOW_WIDTH/2) - point_in_frame_coords[1], WINDOW_HEIGHT-point_in_frame_coords[0])

                if ((WINDOW_WIDTH/2) - point_in_frame_coords[1])>WINDOW_WIDTH or  ((WINDOW_WIDTH/2) - point_in_frame_coords[1])<0 \
                        or (WINDOW_HEIGHT-point_in_frame_coords[0]) < 0 or (WINDOW_HEIGHT-point_in_frame_coords[0]) >WINDOW_HEIGHT:
                    print "off screen", index, ((WINDOW_WIDTH/2) - point_in_frame_coords[1], WINDOW_HEIGHT - point_in_frame_coords[0])

                pygame.draw.lines(screen, (0,255,0), False, [(WINDOW_WIDTH/2,WINDOW_HEIGHT-100), ((WINDOW_WIDTH/2) - point_in_frame_coords[1], WINDOW_HEIGHT - point_in_frame_coords[0]) ], 2)
                #pygame.draw.lines(screen, (255,255,0), False, [(WINDOW_WIDTH/2,WINDOW_HEIGHT), (-2000,-3000) ], 2)
                #pygame.draw.lines(screen, (0,255,0), False, [(rover_center[X],rover_center[Y]-(my_laser.lidar_offset_x*pixel_per_mm_ratio)), (rover_center[X] + int(-single_point[Y]*pixel_per_mm_ratio), rover_center[Y] + int(-single_point[X]*pixel_per_mm_ratio))], 2)
                #TODO:  the lidar height isn't 0, that might make a difference
                #TODO:  why does the lidar not draw correctlly when drawing lines start point lower????


            except Exception as e:
                print "got exception", str(e)


            #Draw a distance overlay if there is a significant change in ranging data
            if abs(transformed_points[index][X] - transformed_points[index-1][X]) > (pixel_delta_threshold_for_drawing_distance) or \
                abs(transformed_points[index][Y] - transformed_points[index - 1][Y]) > (pixel_delta_threshold_for_drawing_distance):

                #calculate distance.  Being sure to include the lidar's physical offset on the rover
                temp_distance = int(math.sqrt( single_point[Y]**2 + (single_point[X]- my_laser.lidar_offset_x)**2 ))
                TextSurf, TextRect = text_objects(str(temp_distance)+"mm", largeText)
                TextRect.center = (rover_center[X] + int(-single_point[Y]*pixel_per_mm_ratio), rover_center[Y] + int(-single_point[X]*pixel_per_mm_ratio))
                screen.blit(TextSurf, TextRect)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        #frontRange = my_laser.currentReadingPolar(-15, 15)
        #print "FRONT RANGE -15, 15:", frontRange

        #leftRange = my_laser.currentReadingPolar(75, 100)
        #print "LEFT RANGE 75, 105:", leftRange

       # rightRange = my_laser.currentReadingPolar(-100, -75)
        #print "RIGHT RANGE -105, -75:", rightRange

        pygame.display.flip()
        clock.tick(240)


    my_laser.shutdown()
