import serial

from hokuyo.driver import hokuyo
from hokuyo.tools import serial_port

# START_STEP = 44
# # STOP_STEP = 725
# START_DEG = 119.885
# STEP_DEG = 0.35208516886930985

uart_port = '/dev/ttyACM0'
uart_speed = 19200

__author__ = 'paoolo'

if __name__ == '__main__':
    laser_serial = serial.Serial(port=uart_port, baudrate=uart_speed, timeout=0.5)
    port = serial_port.SerialPort(laser_serial)

    laser = hokuyo.Hokuyo(port)

    print(laser.laser_on())
    print('---')
    print(laser.get_single_scan())
    print('---')
    print(laser.get_version_info())
    print('---')
    print(laser.get_sensor_specs())
    print('---')
    print(laser.get_sensor_state())
    print('---')
    print(laser.set_high_sensitive())
    print('---')
    print(laser.set_high_sensitive(False))
    print('---')
    print(laser.set_motor_speed(10))
    print('---')
    print(laser.set_motor_speed())
    print('---')
    #print(laser.reset())
    print('---')
    #print(laser.laser_off())

    import time
    last = time.time()

    try:
        # while 1:
        #     if (time.time() - last)>2:
        #         print("single laser scan")
        #         print(laser.get_single_scan())
        #         print('---')
        #         last = time.time()

        #NOTE:  get_single_scan returns a dictionary (unsorted) with pairs of angles in degress and the MM reading
        one_scan = laser.get_single_scan()
        for item in sorted(one_scan):
            print item,one_scan[item]

    except Exception as e:
        print(str(e))
        print(laser.laser_off())