
from __future__ import division # For correct float division in Python 2
from AriaPy import *
import sys
import time
import sys
from MagneticCompass import *
from nav import *
from PID import *
from getGPSdata import *

# WAYPOINT RADIUS in meters (e.g.  we "hit" the waypoint if within this many meters)
WAYPOINT_RADIUS = 2
OBSTACLE_TIMEOUT = 20
OBSTACLE_DISTANCE = 2000

#test_path = [(36.595306 ,-121.875168) ,(36.595112 ,-121.874945) ,(36.595393 ,-121.874979) ,(36.595112 ,-121.874945)] #test triangle
# test_path = [(36.595306,-121.875168),(36.595393,-121.874979),(36.595112,-121.874945)]
# test_path = [(36.595393,-121.874979),(36.595112,-121.874945)]
#test_path = [(36.595306 ,-121.875168) ,(36.595124,-121.875345), (36.596315,-121.876658),( 36.595124,-121.875345)  ]  #south end of quad, north, south

#FINAL DEMO waypoints
#start point, L path to south end of quad, north, south, L Path, start point
test_path = [(36.595306 ,-121.875168) ,( 36.595124,-121.875345), (36.596154,-121.876472),( 36.595124,-121.875345) ,(36.595306 ,-121.875168) ]
#try to stay on the gravel trail and back
#test_path = [(36.5952694185,	-121.8751461515),(36.5952237589,	-121.8752229439), (36.5952546214,	-121.8752778588),(36.5952508812,	-121.8753445074),(36.5952214693,	-121.8754134358),(36.5952204682,	-121.8754882682),(36.5952397020,	-121.8755643296),(36.5952716677,	-121.8756382333),(36.5952397020,	-121.8755643296),(36.5952204682,	-121.8754882682),(36.5952214693,	-121.8754134358),(36.5952508812,	-121.8753445074), (36.5952546214,	-121.8752778588), (36.5952237589,	-121.8752229439),(36.5952694185,	-121.8751461515)]

# Initialize Compass for driving a heading
# compass = MagneticCompass(var=13.28, vardir='E')
compass = MagneticCompass(var=13.28, vardir='E')

# Initialize Nav to calculate heading
navigation = Nav(waypoint_list=test_path)
# Initialize PID Object
my_PID_object = PID(Kp=1.0, Ki=0.5, Kd= -0.2, max_turn=30)
my_GPS_object = GPS(port='/dev/ttyUSB0', baudrate=9600, testing_mode=False)

"""
See the Actions section of the Aria reference manual for more details about
actions, and see the Python README.txt for notes on the tricky aspects
of implementing an ArAction or other subclass.

Also, as a general note, remember actions must take a small amount of time to execute, to avoid
delaying the robot synchronization cycle.
"""



# Action that drives the robot forward, but stops if obstacles are
# detected by sonar.
class ActionGo(ArAction):

    # constructor, sets myMaxSpeed and myStopDistance
    def __init__(self, maxSpeed, stopDistance):
        ArAction.__init__(self, "Go")
        self.myMaxSpeed = maxSpeed
        self.myStopDistance = stopDistance
        self.myDesired = ArActionDesired()
        self.mySonar = None
        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("maximum speed", self.myMaxSpeed, "Maximum speed to go."))
        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("stop distance", self.myStopDistance, "Distance at which to stop."))

    # This fire method is where the real work of the action happens.
    # currentDesired is the combined desired action from other actions
    # previously processed by the action resolver.  In this case, we're
    # not interested in that, we will set our desired
    # forward velocity in the myDesired member, and return it.
    # Note that myDesired must be a class member:, since this method
    # will return a pointer to myDesired to the caller. If we had
    # declared the desired action as a local variable in this method,
    # the pointer we returned would be invalid after this method
    # returned.
    def fire(self, currentDesired):
        # reset the actionDesired (must be done), to clear
        # its previous values.
        self.myDesired.reset()

        # if the sonar is null we can't do anything, so deactivate
        if self.mySonar == None:
            self.deactivate()
            return None

        # get the range of the sonar
        range = self.mySonar.currentReadingPolar(-70, 70) - self.getRobot().getRobotRadius()
        time.sleep(.1)
        range_mod_right = self.mySonar.currentReadingPolar(-120, -70)
        time.sleep(.1)
        range_mod_left = self.mySonar.currentReadingPolar(70, 120)
        # print('Sonar range Right: ', range_mod_right)
        # print('Sonar range left: ', range_mod_left)
        # if the range is greater than the stop distance, find some speed to go
        if (range > self.myStopDistance):
            # just an arbitrary speed based on the range
            speed = range * .3
            # if that speed is greater than our max, cap it
            if (speed > self.myMaxSpeed):
                speed = self.myMaxSpeed
            # now set the velocity
            self.myDesired.setVel(speed)
        else:
            # the range was less than the stop distance, so request stop
            self.myDesired.setVel(0)

        # return a reference to our actionDesired to the resolver to make our request
        return self.myDesired


    # Override setRobot() to get a reference to the sonar device
    def setRobot(self, robot):

        # Set myRobot object in parent ArAction class (must be done if
        # you overload setRobot):
        # self.myRobot = robot
        print "ActionGo: setting robot on ArAction..."
        self.setActionRobot(robot)

        # Find sonar device for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if (self.mySonar == None):
            ArLog.log(ArLog.Terse, "actionExample: ActionGo: Warning: The robot had no sonar range device, deactivating!")
            self.deactivate()


# Action that turns the robot away from obstacles detected by the
# sonar.

class ActionTurn(ArAction):
    def __init__(self, turnThreshold, turnAmount):
        ArAction.__init__(self, "Turn")
        self.myDesired = ArActionDesired()
        self.myTurnThreshold = turnThreshold
        self.myTurnAmount = turnAmount

        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("turn threshold (mm)", self.myTurnThreshold, "The number of mm away from obstacle to begin turnning."))
        # Swig doesn't wrap protected methods yet # self.setNextArgument(ArArg("turn amount (deg)", self.myTurnAmount, "The number of degress to turn if turning."))

        # remember which turn direction we requested, to help keep turns smooth
        self.myTurning = 0 # -1 == left, 1 == right, 0 == none


    def setRobot(self, robot):
        # Sets myRobot in the parent ArAction class (must be done):
        print "ActionTurn: calling ArAction.setActionRobot..."
        self.setActionRobot(robot)
        # self.myRobot = robot


        # Find sonar object for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if (self.mySonar == None):
            ArLog.log(ArLog.Terse, "actionExample: ActionTurn: Warning: I found no sonar, deactivating.")
            self.deactivate()

    def fire(self, currentDesired):

        # reset the actionDesired (must be done)
        self.myDesired.reset()

        # if the sonar is null we can't do anything, so deactivate
        if self.mySonar == None:
            self.deactivate()
            return None

        # Get the left readings and right readings off of the sonar
        leftRange = (self.mySonar.currentReadingPolar(0, 100) -
                     self.getRobot().getRobotRadius())
        rightRange = (self.mySonar.currentReadingPolar(-100, 0) -
                      self.getRobot().getRobotRadius())
        # if neither left nor right range is within the turn threshold,
        # reset the turning variable and don't turn
        if (leftRange > self.myTurnThreshold  and  rightRange > self.myTurnThreshold):
            self.myTurning = 0
            self.myDesired.setDeltaHeading(0)

        # if we're already turning some direction, keep turning that direction
        elif (self.myTurning != 0):
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)

        # if we're not turning already, but need to, and left is closer, turn right
        # and set the turning variable so we turn the same direction for as long as
        # we need to
        elif (leftRange < rightRange):
            self.myTurning = -1
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)

        # if we're not turning already, but need to, and right is closer, turn left
        # and set the turning variable so we turn the same direction for as long as
        # we need to
        else :
            self.myTurning = 1
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)

        # return the actionDesired, so resolver knows what to do
        return self.myDesired



# class for navigation by dead reconing. Works however
# MagneticCompass.py is causing errors and needs to be debugged

class ActionDeadRecon(ArAction):
    def __init__(self):
        ArAction.__init__(self, "DR")
        self.myDesired = ArActionDesired()
        #Added for obstacle avoidance
        self.mySonar = None
        self.timeoutavoid = OBSTACLE_TIMEOUT + 1
        self.obstacle_avoid_heading = None

    def setRobot(self, robot):
        # Sets myRobot in the parent ArAction class (must be done):
        print "ActionDR: calling ArAction.setActionRobot..."
        self.setActionRobot(robot)
        # Find sonar object for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if self.mySonar is None:
            ArLog.log(ArLog.Terse, "ActionDeadRecon: Warning: Unable to find sonar... deactivating.")
            self.deactivate()


    def fire(self, currentDesired):
        # reset the actionDesired (must be done)
        self.myDesired.reset()

        # if the sonar is null we can't do anything, so deactivate
        if self.mySonar is None:
            self.deactivate()
            return None

        compassdata = compass.getcompassdata()
        ###########
        ###########
        #Added code for obstacle avoidance
        if self.timeoutavoid < OBSTACLE_TIMEOUT:
            print "INSIDE OBSTACLE TIMEOUT", self.timeoutavoid, "<", OBSTACLE_TIMEOUT
            print "---Setting absolute heading to obstacle avoid heading"
            self.myDesired.setHeading(self.obstacle_avoid_heading)
            self.timeoutavoid += 1
            return self.myDesired

        range_front_left = self.mySonar.currentReadingPolar(0, 30)
        range_front_right =self.mySonar.currentReadingPolar(-30, 0)

        print 'front ranges >> left:{}\t right:{} '.format(range_front_left, range_front_right)
        if range_front_right < OBSTACLE_DISTANCE and range_front_right < range_front_left:
            print "#################  FOUND OBSTACLE FRONT RIGHT  #################"
            print("Reference HDG", self.myDesired.getHeading())
            self.obstacle_avoid_heading = self.myDesired.getHeading()  + 45
            print("Avoidance HDG", self.obstacle_avoid_heading)
            self.myDesired.setHeading(self.obstacle_avoid_heading)
            self.timeoutavoid = 0
            return self.myDesired

        elif range_front_left < OBSTACLE_DISTANCE:
            print "#################  FOUND OBSTACLE FRONT LEFT  #################"
            self.obstacle_avoid_heading = self.myDesired.getHeading()  - 45
            self.myDesired.setHeading(self.obstacle_avoid_heading)
            self.timeoutavoid = 0
            return self.myDesired

        #End of Adding Code for obstacle avoidance
        #############
        ###########
        # print "Compass Reading", compassdata
        trueheading = compass.trueheading(compassdata)
        # print "True Heading", trueheading

        # desiredheading = float(100.0)  # will need to be updated to get information from GPS at a later time
        current_location = my_GPS_object.getLatLon()  # current location is tuple in Decimal Degrees Lat Lon
        if current_location[0 ] =='' or current_location[1 ] =='':
            print "getLatLon returned empty Lat Lon, aborting this call to fire()"
            return
        target_location = navigation.get_current_waypoint()
        desiredheading = navigation.bearing_between_2_points_decimal_degrees(current_location[0], current_location[1], target_location[0], target_location[1] )
        print "current location:" ,current_location, "\ttarget location:", target_location

        # If we're within
        delta_distance= navigation.distance_between_2_points_decimal_degrees(current_location[0], current_location[1], \
                                                                              target_location[0], target_location[1])
        if delta_distance< WAYPOINT_RADIUS:
            print "Reached Waypoint #", navigation.current_waypoint, " -- heading to next waypoint"
            navigation.current_waypoint+=1
            if navigation.current_waypoint>=len( navigation.waypoint_list):
                print "Mission Complete.  Quitting"
                quit()
            else:
                # just abort this call to fire() and recalculate bearing to new waypoint on next fire() call
                return

        else:
            print "Distance to waypoint", navigation.current_waypoint, "=", delta_distance

        # sending compass and gps data to PID for turn calculation
        temp = my_PID_object.calculate_PID_signal(desired=desiredheading, current=trueheading)
        hdgChange = my_PID_object.plant(temp)

        sentence = 'Current Heading: {}\t Desired Heading: {}\t PID Calculation:: {}\t : Heading Change:{}\t'.format(
            trueheading, desiredheading, temp, hdgChange)
        print(sentence)
        # self.myTurning = math.copysign(1,hdgChange)
        # hdgChange= 15.0
        self.myDesired.setDeltaHeading(hdgChange)
        return self.myDesired


Aria_init()
parser = ArArgumentParser(sys.argv)
parser.loadDefaultArguments()
robot = ArRobot()
conn = ArRobotConnector(parser, robot)
sonar = ArSonarDevice()

# Create instances of the actions defined above, plus ArActionStallRecover,
# a predefined action from Aria_
go = ActionGo(500, 50)
turn = ActionTurn(400, turnAmount=10)
recover = ArActionStallRecover()
dr = ActionDeadRecon()

# Connect to the robot
if not conn.connectRobot():
    ArLog.log(ArLog.Terse, "actionExample: Could not connect to robotnot  Exiting.")
    Aria_exit(1)

# Parse all command-line arguments
if not Aria_parseArgs():
    Aria_logOptions()
    Aria_exit(1)

# Add the range device to the robot. You should add all the range
# devices and such before you add actions
robot.addRangeDevice(sonar)

# Add our actions in order. The second argument is the priority,
# with higher priority actions going first, and possibly pre-empting lower
# priority actions.
# robot.addAction(recover, 100)
robot.addAction(go, 61)
# robot.addAction(turn, 50)
robot.addAction(dr, 60)

# Enable the motors
robot.enableMotors()

# Run the robot processing cycle.
# 'true' means to return if it loses connection,
# after which we exit the program.
robot.run(1)

Aria_exit(0)
