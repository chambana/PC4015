'''
Drew Watson
Homework #1

'''

import math

#DEFINE CLOCKWISE AS 1, COUNTERCLOCKWISE at -1
CW = 1.0
CCW =-1.0

#TEST DATA ONLY.  Lat1/Lon1 is Spanagel Hall, Lat2/Lon2 is the end of the Quad
raw_Lat1='36:35:41.60N'
raw_Lon1='121:52:30.71W'
raw_Lat2='36:35:47.41N'
raw_Lon2='121:52:36.70W'
test_path = [(36.595306,-121.875168),(36.595393,-121.874979),(36.595112,-121.874945)]


class Nav(object):


    def __init__(self, waypoint_list=None):
        self.waypoint_list = waypoint_list
        self.current_waypoint = 0


    def get_current_waypoint(self):
        if self.current_waypoint>=(self.waypoint_list):
            print "No more waypoints!"
            return None
        else:
            return self.waypoint_list[self.current_waypoint]

    #input is of format '36:35:41.60N'
    #output is of format ['36', '35', '41.60', 'N']
    def parse_DegMinSec_to_tuple(self, DMSstring):
        parsed = DMSstring.split(':')
        quadrant = parsed[-1][-1]
        parsed[-1]=parsed[-1][:-1]
        parsed.append(quadrant)
        for i in range(0,3):
            parsed[i]=(float)(parsed[i])
        return parsed

    #input is of format degree, minutes, seconds, and quadrant(?) of the world
    #output is a signed floating point number representing Decimal Degrees
    def DegMinSec_to_DecimalDegrees(self, deg, min, sec, quadrant):
        unsigned_result = deg + min/60.0 + sec/3600.0
        if quadrant == 'S' or quadrant=='W':
            return -unsigned_result
        else:
            return unsigned_result

    #inputs are raw text strings '36:35:41.60N' and get parsed/converted to numeric decimel degress using helper functions
    #output is a floating point distance in meters
    def distance_between_2_points(self, Lat1_DMS_string, Lon1_DMS_string, Lat2_DMS_string, Lon2_DMS_string):

        #parse DMS string to numeric tuple
        Lat1_DMS_tuple = self.parse_DegMinSec_to_tuple(Lat1_DMS_string)
        Lon1_DMS_tuple = self.parse_DegMinSec_to_tuple(Lon1_DMS_string)
        Lat2_DMS_tuple = self.parse_DegMinSec_to_tuple(Lat2_DMS_string)
        Lon2_DMS_tuple = self.parse_DegMinSec_to_tuple(Lon2_DMS_string)

        #convert numeric DMS tuple to floating point Decimal Degree value
        Lat1_dec_deg=self.DegMinSec_to_DecimalDegrees(Lat1_DMS_tuple[0], Lat1_DMS_tuple[1], Lat1_DMS_tuple[2], Lat1_DMS_tuple[3])
        Lon1_dec_deg=self.DegMinSec_to_DecimalDegrees(Lon1_DMS_tuple[0], Lon1_DMS_tuple[1], Lon1_DMS_tuple[2], Lon1_DMS_tuple[3])
        Lat2_dec_deg=self.DegMinSec_to_DecimalDegrees(Lat2_DMS_tuple[0], Lat2_DMS_tuple[1], Lat2_DMS_tuple[2], Lat2_DMS_tuple[3])
        Lon2_dec_deg=self.DegMinSec_to_DecimalDegrees(Lon2_DMS_tuple[0], Lon2_DMS_tuple[1], Lon2_DMS_tuple[2], Lon2_DMS_tuple[3])

        #Use Haversine formulae to calculate distance between 2 points
        Longitude_delta_radians = math.radians(Lon2_dec_deg - Lon1_dec_deg)
        Latitude_delta_radians = math.radians(Lat2_dec_deg - Lat1_dec_deg)
        Latitude1_radians = math.radians(Lat1_dec_deg)
        Latitude2_radians = math.radians(Lat2_dec_deg)

        a =  (math.sin(Latitude_delta_radians/2.0)**2) + \
             (math.cos(Latitude1_radians)*math.cos(Latitude2_radians)) * \
             (math.sin(Longitude_delta_radians/2.0)**2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        r = 6371000
        d = r *c

        return d

    def distance_between_2_points_decimal_degrees(self, Lat1_dec_deg,  Lon1_dec_deg,  Lat2_dec_deg,  Lon2_dec_deg):

        #Use Haversine formulae to calculate distance between 2 points
        Longitude_delta_radians = math.radians(Lon2_dec_deg - Lon1_dec_deg)
        Latitude_delta_radians = math.radians(Lat2_dec_deg - Lat1_dec_deg)
        Latitude1_radians = math.radians(Lat1_dec_deg)
        Latitude2_radians = math.radians(Lat2_dec_deg)

        a =  (math.sin(Latitude_delta_radians/2.0)**2) + \
             (math.cos(Latitude1_radians)*math.cos(Latitude2_radians)) * \
             (math.sin(Longitude_delta_radians/2.0)**2)
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        r = 6371000
        d = r *c

        return d

    #input format is:  startpoint Lat, startpoint Lon, Target Location Lat, Target Location Lon.  All inputs are strings
    #output format is bearing angle in degrees (floating point)
    def bearing_between_2_points(self, Lat1_DMS_string, Lon1_DMS_string, Lat2_DMS_string, Lon2_DMS_string):
        # parse DMS string to numeric tuple
        Lat1_DMS_tuple = self.parse_DegMinSec_to_tuple(Lat1_DMS_string)
        Lon1_DMS_tuple = self.parse_DegMinSec_to_tuple(Lon1_DMS_string)
        Lat2_DMS_tuple = self.parse_DegMinSec_to_tuple(Lat2_DMS_string)
        Lon2_DMS_tuple = self.parse_DegMinSec_to_tuple(Lon2_DMS_string)

        # convert numeric DMS tuple to floating point Decimal Degree value
        Lat1_dec_deg = self.DegMinSec_to_DecimalDegrees(Lat1_DMS_tuple[0], Lat1_DMS_tuple[1], Lat1_DMS_tuple[2],
                                                   Lat1_DMS_tuple[3])
        Lon1_dec_deg = self.DegMinSec_to_DecimalDegrees(Lon1_DMS_tuple[0], Lon1_DMS_tuple[1], Lon1_DMS_tuple[2],
                                                   Lon1_DMS_tuple[3])
        Lat2_dec_deg = self.DegMinSec_to_DecimalDegrees(Lat2_DMS_tuple[0], Lat2_DMS_tuple[1], Lat2_DMS_tuple[2],
                                                   Lat2_DMS_tuple[3])
        Lon2_dec_deg = self.DegMinSec_to_DecimalDegrees(Lon2_DMS_tuple[0], Lon2_DMS_tuple[1], Lon2_DMS_tuple[2],
                                                   Lon2_DMS_tuple[3])

        #calculate deltas (in radians)
        Longitude_delta_radians = math.radians(Lon2_dec_deg - Lon1_dec_deg)
        Latitude1_radians = math.radians(Lat1_dec_deg)
        Latitude2_radians = math.radians(Lat2_dec_deg)

        #use atan2 to get a signed result across all 4 quadrants
        angle_rad = math.atan2( math.sin(Longitude_delta_radians) * math.cos(Latitude2_radians), math.cos(Latitude1_radians) * math.sin(Latitude2_radians) \
                                - math.sin(Latitude1_radians) * math.cos(Latitude2_radians) * math.cos(Longitude_delta_radians) )


        return math.degrees(angle_rad)


    # input format is:  startpoint Lat, startpoint Lon, Target Location Lat, Target Location Lon.  All inputs are strings
    # output format is bearing angle in degrees (floating point)
    def bearing_between_2_points_decimal_degrees(self, Lat1_dec_deg, Lon1_dec_deg, Lat2_dec_deg, Lon2_dec_deg):
        # calculate deltas (in radians)
        Longitude_delta_radians = math.radians(Lon2_dec_deg - Lon1_dec_deg)
        Latitude1_radians = math.radians(Lat1_dec_deg)
        Latitude2_radians = math.radians(Lat2_dec_deg)

        # use atan2 to get a signed result across all 4 quadrants
        angle_rad = math.atan2(math.sin(Longitude_delta_radians) * math.cos(Latitude2_radians),
                               math.cos(Latitude1_radians) * math.sin(Latitude2_radians) \
                               - math.sin(Latitude1_radians) * math.cos(Latitude2_radians) * math.cos(
                                   Longitude_delta_radians))

        return math.degrees(angle_rad)


    #input:  your current heading in degrees (pos or neg) and your target's magnetic bearing in degrees (pos or neg)
    #output:  returns 1 for CW, -1 for CCW
    def calculate_turn_direction(self, my_heading, target_bearing):
        if my_heading<0:
            my_heading=360-abs(my_heading)
        if target_bearing<0:
            target_bearing=360-abs(target_bearing)
        heading_difference = target_bearing - my_heading
        if abs(heading_difference) < 180:
            return math.copysign(1,heading_difference)
        else:
            return -math.copysign(1,heading_difference)


if __name__ == "__main__":

    nav_object = Nav()

    #Compute distance (meters) between two lat/lon in DEG:MIN:SEC
    print "Distance (meters) between Spanagel and end of the quad:", \
        nav_object.distance_between_2_points(raw_Lat1,raw_Lon1, raw_Lat2, raw_Lon2)

    #Compute heading (degrees) betwen two lat/lon in DEG:MIN:SEC
    print "Bearing (degrees) from Spanagel to end of the quad:", nav_object.bearing_between_2_points(raw_Lat1, raw_Lon1,raw_Lat2,raw_Lon2),"\n"

    #do it in dec deg
    print "Bearing (degrees) for test path (should be 60):", \
        nav_object.bearing_between_2_points_decimal_degrees(test_path[0][0],test_path[0][1], test_path[1][0], test_path[1][1]),"\n"


    #Compute the most efficient turn direction (CW=1 CCW=-1)
    for i in range(0,360,90):
        print "If robot is oriented with heading",i,"degrees at Spanagel, the shortest turn direction to the library is:"
        direction=nav_object.calculate_turn_direction(my_heading=i, target_bearing=-39)
        if direction==CCW:
            print "CCW"
        else:
            print "CW"

    print "\n"
    #Compute the most efficient turn direction (CW=1 CCW=-1)
    for i in range(0, 360, 90):
        print "If robot is oriented with heading", i, "degrees at Spanagel, the shortest turn direction to Sloat Gate is:"
        direction = nav_object.calculate_turn_direction(my_heading=i, target_bearing=234)
        if direction == CCW:
            print "CCW"
        else:
            print "CW"







