


class CircularBuffer(object):

    def __init__(self, number_of_saved_error_terms=10):
        self.circular_buffer = [0] * number_of_saved_error_terms
        self.index = 0
        self.size = number_of_saved_error_terms

    def insert(self, item):
        self.circular_buffer[self.index%self.size]=item
        self.index=self.index+1

    def sum(self):
        sum = 0
        for item in self.circular_buffer:
            sum+=item
        return sum

    def integral(self):
        #alias for sum()
        return self.sum()

    def average(self):
        return self.sum() / self.size

    def derivative(self, time_delta = 1):
        #return (self.circular_buffer[-1] - self.circular_buffer[-2]) / (time_delta)
        return (self.circular_buffer[(self.index-1)%self.size] - self.circular_buffer[(self.index-2)%self.size]) / (time_delta)


    def __str__(self):
        return str(self.circular_buffer)

    def clear(self):
        self.circular_buffer = [0] * self.size

if __name__ == "__main__":

    #TESTING OF CLASS

    my_buffer = CircularBuffer(number_of_saved_error_terms=5)

    for i in range(1,10):
        print(my_buffer, "\tsum:",my_buffer.sum(), "\taverage:", my_buffer.average())
        my_buffer.insert(i)

    print(my_buffer)
    my_buffer.clear()
    print(my_buffer)
