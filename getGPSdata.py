import os
import math
from datetime import datetime
import serial
import time
import threading
import Queue


#inputString = '$GPGGA,123519,4807.0380,S,01131.000,W,1,08,0.9,545.4,M,46.9,M,,*47'
inputString = '$GPGGA,222410.00,3635.72099,N,12152.49394,W,2,05,2.75,4.8,M,-31.1,M,,*6D'
# inputString format
'''
$GPGGA, <1>,<2>,<3>,<4>,<5>,<6>,<7>,<8>,<9>,M,<10>,<11>,<12>*hh<CR>,<LF>
<1> UTC time of position fix, hhmmss format
<2> Latitude, ddmm.mmmm format
<3> Latitude hemisphere, N or S
<4> Longitude, ddmm.mmmm format
<5> Longitude hemipshere, E or W
<6> GPS, quality indication, 0 = not available, 1 = Non-differential, 2 = DGPS, 6 = estimate
<7> Number of Satellites in use, 00-12
<8> Horizontal dilution of precision, 0.5 - 99.9
<9> Antenna height above/below mean sea level -9999.9 to 99999.9 meters
<10> Geoidal height, -999.9 to 9999.9
<11> Differential GPS data age
<12> Differential reference station ID 0000 to 1023
'''

# input txt file raw GPS data
inFile = 'GGAstring.txt'
'''
os.chdir('/Users/student/')
os.environ.get('HOME')
file_path = os.path.join(os.environ.get('HOME'),inFile)
os.path.exists(file_path)
os.path.isdir()
os.path.isfile()
os.path.splitext()
#what's available
print(dir(os.path))

with open(file_path, 'w')as f:
    f.write('writing to file')
'''



class GPS(object):

    def __init__(self, port = '/dev/ttyUSB0', baudrate = 9600, testing_mode = False):

        self.latest_lat_lon=None
        if testing_mode==False:
            try:
                print "Attempting to open",port,"at baudrate", baudrate
                self.gps_device = serial.Serial(port, baudrate)
                time.sleep(2)
                self.latlon_queue = Queue.LifoQueue()
                self._stopevent = threading.Event()
                self.thread2=threading.Thread(target=self.infinite_loop_nmea_grabber, args=(self.latlon_queue,))
                self.thread2.start()
            except Exception as e:
                print "Had an issue opening your device, is it plugged in and connected to", port, \
                    "with baudrate", baudrate, "?"
                print "Error Code: ", str(e)
                print "Plug in GPS hardware or run in Testing_Mode"
                quit(1)


    def infinite_loop_nmea_grabber(self, data_transfer_queue):
        print "Started infinite loop for handling GPS hardware in separate thread"
        while not self._stopevent.isSet():
            try:
                #print "thread 2 is grabbing"
                raw_nmea_sentence = self.__get_raw_NMEA_sentence()
                parsed_nmea_sentence = self.parseGPS(raw_nmea_sentence)
                converted_dd_nmea_sentence = self.convertDMS_to_DD(parsed_nmea_sentence)
                data_transfer_queue.put((converted_dd_nmea_sentence['Latitude'], converted_dd_nmea_sentence['Longitude']))
                #print "thread 2 got", (converted_dd_nmea_sentence['Latitude'], converted_dd_nmea_sentence['Longitude'])
                #print "queue size:", data_transfer_queue.qsize()
                time.sleep(0.5)
            except:
                print "Exception in hardware handling thread, quitting"
                self.gps_device.close()
                quit(1)

    #returns in a Decimal Degree tuple of lat lon
    def getLatLon(self):
        ''' '''
        # raw_nmea_sentence = self.__get_raw_NMEA_sentence()
        # parsed_nmea_sentence = self.parseGPS(raw_nmea_sentence)
        # converted_dd_nmea_sentence = self.convertDMS_to_DD(parsed_nmea_sentence)
        # return (converted_dd_nmea_sentence['Latitude'],converted_dd_nmea_sentence['Longitude'])
        if self.latlon_queue.qsize()!=0:
            self.latest_lat_lon= self.latlon_queue.get()
            #print "main thread got this",self.latest_lat_lon
        else:
            pass
            #print "no new data since last update, using last measurement"
        return self.latest_lat_lon

    def __get_raw_NMEA_sentence(self):
        '''Read GPS NMEA sentence'''
        raw_nmea_sentence = self.gps_device.readline()
        while raw_nmea_sentence[0:6] != '$GPGGA':
            raw_nmea_sentence = self.gps_device.readline()
        return raw_nmea_sentence



    #input GGA string
    #return GPS dictionary
    def parseGPS(self, rawGPS):
        """
        :param rawGPS: GGA string input
        :return: GGA dict
        """
        tempGPS = rawGPS.split(',')
        d1 = {'UTC': tempGPS[1],
              'Latitude': tempGPS[2],
              'Lat_Hem': tempGPS[3],
              'Longitude': tempGPS[4],
              'Long_Hem': tempGPS[5],
              'GPS_quality': int(tempGPS[6]),
              'Num_Sats_used': int(tempGPS[7]),
              'Horiz_dilut': float(tempGPS[8]) if tempGPS[8] else None,
              #'Anten_height_MSL': float(tempGPS[9]),
               'Anten_height_MSL': float(tempGPS[9]) if tempGPS[9] else None,  #can't cast None to float, throws exception
              'GeoHeight': tempGPS[10], #cast to float later?
              'Dfrn_data_age': tempGPS[11],
              'Dfrn_ref_id': tempGPS[12]}
        return d1


    def getGPS_file(self):
        try:
            with open(inFile, 'r')as f:
                f_contents = f.readline()
                print('Reading file: ' + f.name)
                print(f_contents)
        except Exception as e:
            print(e)
            f = open(inFile, 'a')
            print('Create new file' + inFile)
            f.close()
        else:
            wp=self.parseGPS(f_contents)
            print('wp dictionary created')
            print(wp)
        #TODO CLEAN UP LAT LONG COORDS
        # def convertDMS_to_DD()
        finally:
            pass

    def printWPfile(self):
        pass

    def add_new_wp(wpfile, lat, lon):
        wpLat = str(lat)
        wpLon = str(lon)
        f = open(wpfile,'a')
        f.write(wpLat)
        f.write('\t')
        f.write(wpLon)
        f.write('\n')
        f.close()
        print('Waypoint added to file')
        pass

    def convertDMS_to_DD(self,dict):
        '''
        unpack wp_raw
        iterate key value in for wp_raw.items():

        :return:
        '''
#        dict = Dictionary
        lat = dict.get('Latitude')
        lon = dict.get('Longitude')
        if lat==None or lat == '' or lon==None or lon == '':
            print "Couldn't convert empty lat/lon to decimal degress!"
            return dict
        dms_coord_str = 'For DMS: lat = {Latitude} and long = {Longitude}'.format(**dict)
        print(dms_coord_str)

        unsigned_lat = float(lat[0:2])+ float(lat[2:9])/60
        unsigned_lon = float(lon[0:3])+ float(lon[3:9])/60
        if dict.get('Lat_Hem')== 'S':
            unsigned_lat = -unsigned_lat

        elif dict.get('Long_Hem')=='W':
            unsigned_lon = -unsigned_lon
        print(unsigned_lat)
        print(unsigned_lon)

        #update dict
        dict['Latitude'] = unsigned_lat
        dict['Longitude'] = unsigned_lon

        return dict


if __name__ == "__main__":
    my_GPS_object = GPS(port='/dev/ttyUSB4', baudrate=9600, testing_mode=False)
    print('Raw GPS string: '+ inputString)
    GPSdict = my_GPS_object.parseGPS(inputString)
    print "GPS dict", GPSdict
    my_GPS_object.convertDMS_to_DD(GPSdict)

        #for i in range(0,100):
    while 1:
        try:
            print "Conducting 'NO WAIT' grab of Lat Lon... "
            print "reading:", my_GPS_object.getLatLon()
            time.sleep(.3)
        except:
            print "asking thread2 to die"
            my_GPS_object._stopevent.set()
            break


