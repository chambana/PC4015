from hw3_integration_buffer import CircularBuffer
import time
"""

PID control for mobile robot

S= (Kp * E) + (Ki * INTEGRALde) - (Kd * de/dt)

"""

#CONSTANTS
INTEGRAL_RANGE = 5  #number of saved error terms to use to calculate the I term integral
Kp = 0.5
Ki = 0.1
Kd = 0.0

DESIRED_HEADING = 100
ACTUAL_HEADING = 15

if __name__ == "__main__":

    error_term_history = CircularBuffer(number_of_saved_error_terms=5)
    E = DESIRED_HEADING - ACTUAL_HEADING
    print("DESIRED HEADING:", DESIRED_HEADING, "\tACTUAL HEADING: ", ACTUAL_HEADING,"\tinitial E", E)

    #TODO Clear integral terms

    while True:
        S = (Kp * E) + (Ki * error_term_history.integral()) + (Kd * error_term_history.derivative())
        #S = E+S
        ACTUAL_HEADING = ACTUAL_HEADING + S
        E = DESIRED_HEADING - ACTUAL_HEADING  # E = PV - SP
        print("Error:"+str(E),"Signal:"+str(S), "Error History:"+str(error_term_history), "integral:"+str(error_term_history.integral())\
              ,"derivative:"+str(error_term_history.derivative()))
        error_term_history.insert(E)
        time.sleep(1)
