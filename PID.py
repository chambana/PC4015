from hw3_integration_buffer import CircularBuffer

DISTANCE_RANGE = 4500.0


class PID(object):
    # Base constructor(self, ProportionConst, IntegralConst, DifferentialConst)
    def __init__(self, Kp, Ki, Kd, max_turn):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.error_term_history = CircularBuffer(number_of_saved_error_terms=5)
        self.max_turn_angle_magnitude = max_turn

    @property
    def __str__(self):
        return 'Gain coefficients: Kp:{}\t Ki:{}\t Kd:{}\t Max Turn Angle(deg):{}'.format(self.Kp, self.Ki, self.Kd,
                                                                                          self.max_turn_angle_magnitude)

    
    def calculate_PID_signal(self, desired, current):
        Error = current - desired # positve turns left, negitive turns right
        # restrict error values to between 180 and -180. This will cause all turns to be shortest direction and eliminate turn reversal at 360
        if Error > 180.0:
            Error = Error - 360.0
        if Error < - 180.0:
            Error = Error + 360.0
        #print('Printing error',Error)
        Signal = (self.Kp * Error) + (self.Ki * self.error_term_history.integral()) + (
        self.Kd * self.error_term_history.derivative())
        self.error_term_history.insert(Error)

        # self.adjusted_DISTANCE_RANGE = DISTANCE_RANGE - desired
        return Signal


    def plant(self, signal):
        '''
        This function takes a unitless PID output and produces a linearly scaled delta heading value
        :param signal:
        :return:
        '''
        # linear mapping of output from PID (the signal is a numeric value without units) to a turn angle
        turn_range = self.max_turn_angle_magnitude
        linScalingFactor = turn_range / 180.0
        demanded_turn = linScalingFactor * signal
        #ratio_of_degrees_to_mm = turn_range / self.adjusted_DISTANCE_RANGE
        #demanded_turn = ratio_of_degrees_to_mm * signal

        return demanded_turn


if __name__ == "__main__":
    print "Testing PID class"
    my_PID_object = PID(Kp=1, Ki=0, Kd=0, max_turn=60)
    print(my_PID_object.__str__)
    # signal= 100

    # for signal in range(0,4600, 100):
    #     print "Signal = ", signal
    #     print "plant output = ", my_PID_object.plant(signal)

    desired = 400
    # current = 300

    for current in range(0, 4600, 100):
        temp = my_PID_object.calculate_PID_signal(desired=desired, current=current)
        print "PID calc: {0}\t desired:{1}\t current:{2}\tplant output(deg):{3}".format(temp,desired,current,my_PID_object.plant(temp))
        #print "corresponding plant out (in degrees turn angle): ", my_PID_object.plant(temp)
