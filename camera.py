import pygame
import pygame.camera
from pygame.locals import *
#
# pygame.init()
# pygame.camera.init()

import pygame
from math import *

CAMERA_MOUNT_ANGLE_DEG = 45.0
CAMERA_MOUNT_HEIGHT_MM = 965.2 #38 inches
HFOV_DEG = 70.42 #hard coded to Logitech c920
VFOV_DEG = 43.30 #hard coded to Logitech c920
CAMERA_WIDTH_PIXELS = 1920
CAMERA_HEIGHT_PIXELS = 1080


#
# For the c920, with 16:9 sensor:
# for 16:9 resolutions: DFOV=78, HFOV=70.42 and VFOV=43.30
#TODO:  get deg to pixel ratio
#TODO:  how far do up on the screen do you draw the line

class Capture(object):
    def __init__(self):
        self.size = (CAMERA_WIDTH_PIXELS,CAMERA_HEIGHT_PIXELS)
        # create a display surface. standard pygame stuff
        self.display = pygame.display.set_mode(self.size, 0)

        # this is the same as what we saw before
        pygame.camera.init()

        self.clist = pygame.camera.list_cameras()
        if not self.clist:
            raise ValueError("Sorry, no cameras detected.")
        self.cam = pygame.camera.Camera(self.clist[0], self.size)
        self.cam.start()

        # create a surface to capture to.  for performance purposes
        # bit depth is the same as that of the display surface.
        self.snapshot = pygame.surface.Surface(self.size, 0, self.display)


        self.vert_pixel_to_deg_ratio = CAMERA_HEIGHT_PIXELS / VFOV_DEG  #pixels per degree
        self.horiz_pixel_to_deg_ratio = CAMERA_WIDTH_PIXELS / HFOV_DEG #pixels per degree


        lower_hypotenuse_length = (CAMERA_MOUNT_HEIGHT_MM) / (cos(radians(CAMERA_MOUNT_ANGLE_DEG - (VFOV_DEG /2))))
        upper_hypotenuse_length = (CAMERA_MOUNT_HEIGHT_MM) / (cos(radians(CAMERA_MOUNT_ANGLE_DEG + (VFOV_DEG /2))))

        self.lowerhalfwidth = lower_hypotenuse_length * tan(radians(HFOV_DEG/2))
        self.upperhalfwidth = upper_hypotenuse_length * tan(radians(HFOV_DEG/2))

        self.upperlength = CAMERA_MOUNT_HEIGHT_MM * tan(radians(CAMERA_MOUNT_ANGLE_DEG + (VFOV_DEG/2)))
        self.lowerlength = CAMERA_MOUNT_HEIGHT_MM * tan(radians(CAMERA_MOUNT_ANGLE_DEG - (VFOV_DEG/2)))

        self.lower_left = (-self.lowerhalfwidth, self.lowerlength)
        self.lower_right = (self.lowerhalfwidth, self.lowerlength)
        self.upper_left = (-self.upperhalfwidth, self.upperlength)
        self.upper_right = (self.upperhalfwidth, self.upperlength)

        print "ground location of lower left of camera frame",self.lower_left
        print "ground location of lower right of camera frame",self.lower_right
        print "ground location of upper left of camera frame",self.upper_left
        print "ground location of upper right of camera frame",self.upper_right


        # for x_distance in range(400, 2300, 500):
        #     for y_distance in range(-100, 200, 100):
        #         print x_distance, y_distance
        #         self.projection(x_distance, y_distance)



    def projection(self, point_ground_x, point_ground_y):

        point_angle_from_axis_radians = atan(point_ground_x / CAMERA_MOUNT_HEIGHT_MM)
        point_angle_from_axis_degrees = degrees(point_angle_from_axis_radians)

        angle_from_axis_to_bottom_of_frame_deg = CAMERA_MOUNT_ANGLE_DEG - (VFOV_DEG/2)

        angle_from_bottom_of_frame_to_point_deg = point_angle_from_axis_degrees - angle_from_axis_to_bottom_of_frame_deg

        num_pixels_from_bottom_of_frame = angle_from_bottom_of_frame_to_point_deg * self.vert_pixel_to_deg_ratio

        #print "vertical offset in pix from bottom of frame", num_pixels_from_bottom_of_frame

        distance_to_point_from_camera_x_component = CAMERA_MOUNT_HEIGHT_MM / cos(point_angle_from_axis_radians)

        horiz_angle_from_centerline_rad = atan(point_ground_y / distance_to_point_from_camera_x_component )

        horiz_angle_from_centerline_deg = degrees(horiz_angle_from_centerline_rad)

        num_y_pixels_from_centerline = horiz_angle_from_centerline_deg * self.horiz_pixel_to_deg_ratio

        #print "horizontal offset in pix from center of frame", num_y_pixels_from_centerline

        return (num_pixels_from_bottom_of_frame, num_y_pixels_from_centerline)


    def get_and_flip(self):
        # if you don't want to tie the framerate to the camera, you can check
        # if the camera has an image ready.  note that while this works
        # on most cameras, some will never return true.
        if self.cam.query_image():
            self.snapshot = self.cam.get_image(self.snapshot)

#        pygame.draw.line(self.snapshot, (0,255,0), (CAMERA_WIDTH_PIXELS/2, CAMERA_HEIGHT_PIXELS), (120, 60), 4)

        # for x_distance in range(400, 2300, 500):
        #     for y_distance in range(-100, 200, 100):
        #         #print x_distance, y_distance
        #         ground_location_projected_on_frame = self.projection(x_distance, y_distance)
        #         pygame.draw.line(self.snapshot, (0, 255, 0), (CAMERA_WIDTH_PIXELS / 2, CAMERA_HEIGHT_PIXELS), ((CAMERA_WIDTH_PIXELS/2) + ground_location_projected_on_frame[1], CAMERA_HEIGHT_PIXELS - ground_location_projected_on_frame[0]), 4)

        # blit it to the display surface.  simple!
        self.display.blit(self.snapshot, (0,0))
        #pygame.display.flip()

    def main(self):
        going = True
        while going:
            events = pygame.event.get()
            for e in events:
                if e.type == QUIT or (e.type == KEYDOWN and e.key == K_ESCAPE):
                    # close the camera safely
                    self.cam.stop()
                    going = False

            self.get_and_flip()


if __name__ == "__main__":
    mycam = Capture()
    mycam.main()