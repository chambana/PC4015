#!/usr/bin/python
'''
File: finalproject.py
Version: 1.0
Last-Modified: 15-Feb-2017
Src Refactored: FinalProject.py
Authors: Drew Watson <akwatson@nps.edu>,
        LT Heather Tilley <hptilley@nps.edu>,
        LT Edward Wulff <evwulff@nps.edu>
Interpreter: Python 2.7
Status: Not fully Implements NAV.py
Purpose: Implement Autonomous functioning on Pioneer platform robot
Created: 10-Mar-2017
Post-History@Sakai: none
'''

#imports
from __future__ import division  # For correct float division in Python 2
from AriaPy import *
import sys
import subprocess
import time
import sys
from MagneticCompass import *
#from nav import *
from NAV import *
from PID import *
from GPS import *

# WAYPOINT RADIUS in meters (e.g.  we "hit" the waypoint if within this many meters)
WAYPOINT_RADIUS = 1
test_path = [(36.595306, -121.875168), (36.595112, -121.874945), (36.595393, -121.874979), (36.595112, -121.874945)]

# Initialize Sensor Devices
compass = MagneticCompass(var=13.28, vardir='E')
my_GPS_object = GPS(port='/dev/ttyUSB0', baudrate=9600, testing_mode=False)
# Initialize Supervisor modules
# Nav -> waypoint
navigation = Nav(waypoint_list=test_path)
# PID Object -> interfaces with robot hardware, Aria code
my_PID_object = PID(Kp=2.0, Ki=0, Kd=0, max_turn=30)




# Action drives robot forward, but stops if obstacles are detected by sonar.
class ActionGo(ArAction):
    """ constructor, sets myMaxSpeed and myStopDistance"""
    def __init__(self, maxSpeed, stopDistance):
        ArAction.__init__(self, "Go")
        self.myMaxSpeed = maxSpeed
        self.myStopDistance = stopDistance
        self.myDesired = ArActionDesired()
        self.mySonar = None

    # Override setRobot() gets reference to sonar device
    def setRobot(self, robot):
        """required override Parent class ArAction"""
        print "ActionGo: setting robot on ArAction..."
        self.setActionRobot(robot)
        # Find sonar device for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if self.mySonar == None:
            ArLog.log(ArLog.Terse,
                      "ActionGo: Warning: Unable to detect sonar range device, deactivating!")
            self.deactivate()

    # Driving Logic method fire()
    def fire(self, currentDesired):
        """overrides ActionGo attr myDesired, to clear ->reset()
        required to clear previous values"""
        self.myDesired.reset()

        if self.mySonar == None:
            self.deactivate()
            return None
        # get the range of the sonar
        # GLOBAL CONSTANTS FOR DEGREE START STOP USED BY SONAR
        range = self.mySonar.currentReadingPolar(-70, 70) - self.getRobot().getRobotRadius()
        time.sleep(.1)
        range_mod_right = self.mySonar.currentReadingPolar(-120, -70)
        time.sleep(.1)
        range_mod_left = self.mySonar.currentReadingPolar(70, 120)

        # if sonar range value is greater than the stop [STANDOFF] distance
        if range > self.myStopDistance:
            # modify speed since robot closing [STANDOFF] dist
            curr_speed = range * .3  # RENAME ARB_SPEED GLOBAL USE
            # if that speed is greater than our max, cap it
            if curr_speed > self.myMaxSpeed:
                curr_speed = self.myMaxSpeed
            # now set the velocity
            self.myDesired.setVel(curr_speed)
        else:
            # the range was less??GREATER[encroached standoff dist] than the stop distance, so request stop
            self.myDesired.setVel(0)
        # return reference notifies actionDesired resolver of request
        return self.myDesired

class ActionDeadRecon(ArAction):
    def __init__(self):
        ArAction.__init__(self, "DR")
        self.myDesired = ArActionDesired()
        # Added for obstacle avoidance
        self.mySonar = None
        self.timeoutavoid = OBSTACLE_TIMEOUT + 1
        self.obstacle_avoid_heading = None

    def setRobot(self, robot):
        # Sets myRobot in the parent ArAction class (must be done):
        print "ActionDR: calling ArAction.setActionRobot..."
        self.setActionRobot(robot)
        # Find sonar object for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if self.mySonar is None:
            ArLog.log(ArLog.Terse, "ActionDeadRecon: Warning: Unable to find sonar... deactivating.")
            self.deactivate()

    def fire(self, currentDesired):
        # reset the actionDesired (must be done)
        self.myDesired.reset()

        # if the sonar is null we can't do anything, so deactivate
        if self.mySonar is None:
            self.deactivate()
            return None


        print("Reference HDG", self.myDesired.getHeading())
        self.obstacle_avoid_heading = self.myDesired.getHeading() - 45
        print("Avoidance HDG", self.obstacle_avoid_heading)
        self.myDesired.setHeading(self.obstacle_avoid_heading)
        return self.myDesired



        # Pull Compass Data
        compassdata = compass.getcompassdata()

        ###########
        ###########
        # Added code for obstacle avoidance






# class for navigation by dead reconing. Works however
# MagneticCompass.py is causing errors and needs to be debugged







Aria_init()
parser = ArArgumentParser(sys.argv)
parser.loadDefaultArguments()
robot = ArRobot()
conn = ArRobotConnector(parser, robot)
sonar = ArSonarDevice()
# Create instances of the actions defined above, plus ArActionStallRecover,
# a predefined action from Aria_
go = ActionGo(500, 50)
#turn = ActionTurn(400, turnAmount=10)
#recover = ArActionStallRecover()
dr = ActionDeadRecon()
#avoid = ActionAvoid(500, 50)

# Connect to the robot
if not conn.connectRobot():
    ArLog.log(ArLog.Terse, "actionExample: Unable to connect to robot.  Exiting...")
    Aria_exit(1)

# Parse all command-line arguments
if not Aria_parseArgs():
    Aria_logOptions()
    Aria_exit(1)
# Add the range device to the robot before adding any actions
robot.addRangeDevice(sonar)
# Add our actions in order. The second argument is the priority,
# with higher priority actions going first, and possibly pre-empting lower
# priority actions.

#Add Actions to robot after registering range devices -> robot.addRangeDevice(devicename=sonar)
def addActions(self):
    """Add user defined actions in sequence order (i.e Desired Task priority and associated priority value)
    :argument name of ArAction subclass instance (e.g. go=ActionGo(ArAction)...
    :argument priority :type int [between 0-99]?
    note: higher valued priorities may possibly pre-empting lower and unexpected behavior"""

    # robot.addAction(recover, 100)
    robot.addAction(go, 61)
    # robot.addAction(turn, 50)
    robot.addAction(dr, 60)
    #robot.addAction(avoid, 59)
    ##TODO validate no side-effects from commented addActions -> turn, recover


# Enable the motors
robot.enableMotors()
# Run the robot processing cycle.
# 'true' means to return if it loses connection,
# after which we exit the program.
robot.run(1)
##TODO prompt if user wants to run teleop.mode
while(1):
    usrchoice = raw_input("Press any key to run Teleop mode. Otherwise press q to quit \n")
    if usrchoice =="q":
        print('Exiting. Please wait')
        time.sleep(2)
        exit()
    else:
        try:
            #mode = subprocess.Popen([sys.executable,"Teleop.py"]) #windows
            subprocess.call(['python','teleop.py'])
            os.chdir('/Users/student/')
            os.environ.get('HOME')
            inFile = 'teleop.py'
            file_path = os.path.join(os.environ.get('HOME'),inFile)
            if os.path.exists(file_path)==True:
                subprocess.call('./teleop.py')
            #Aria\pythonExamples
        except Exception as e:
            print str(e)
        else:
            print"Error in attempt to run File: teleop.py"

Aria_exit(0)
