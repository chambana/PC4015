#!/usr/bin/python
'''
File: finalproject.py
Version: 1.0
Last-Modified: 15-Feb-2017
Src Refactored: FinalProject.py
Authors: Drew Watson <akwatson@nps.edu>,
        LT Heather Tilley <hptilley@nps.edu>,
        LT Edward Wulff <evwulff@nps.edu>
Interpreter: Python 2.7
Status: Not fully Implements NAV.py
Purpose: Implement Autonomous functioning on Pioneer platform robot
Created: 10-Mar-2017
Post-History@Sakai: none
'''

#imports
from __future__ import division  # For correct float division in Python 2
from AriaPy import *
import sys
import subprocess
import time
import sys
from MagneticCompass import *
#from nav import *
from NAV import *
from PID import *
from GPS import *

# WAYPOINT RADIUS in meters (e.g.  we "hit" the waypoint if within this many meters)
WAYPOINT_RADIUS = 1
test_path = [(36.595306, -121.875168), (36.595112, -121.874945), (36.595393, -121.874979), (36.595112, -121.874945)]

# Initialize Sensor Devices
compass = MagneticCompass(var=13.28, vardir='E')
my_GPS_object = GPS(port='/dev/ttyUSB0', baudrate=9600, testing_mode=False)
# Initialize Supervisor modules
# Nav -> waypoint
navigation = Nav(waypoint_list=test_path)
# PID Object -> interfaces with robot hardware, Aria code
my_PID_object = PID(Kp=2.0, Ki=0, Kd=0, max_turn=30)


# Action drives robot forward, but stops if obstacles are detected by sonar.
class ActionGo(ArAction):
    """ constructor, sets myMaxSpeed and myStopDistance"""
    def __init__(self, maxSpeed, stopDistance):
        ArAction.__init__(self, "Go")
        self.myMaxSpeed = maxSpeed
        self.myStopDistance = stopDistance
        self.myDesired = ArActionDesired()
        self.mySonar = None

    # Override setRobot() gets reference to sonar device
    def setRobot(self, robot):
        """required override Parent class ArAction"""
        print "ActionGo: setting robot on ArAction..."
        self.setActionRobot(robot)
        # Find sonar device for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if self.mySonar == None:
            ArLog.log(ArLog.Terse,
                      "ActionGo: Warning: Unable to detect sonar range device, deactivating!")
            self.deactivate()

    # Driving Logic method fire()
    def fire(self, currentDesired):
        """overrides ActionGo attr myDesired, to clear ->reset()
        required to clear previous values"""
        self.myDesired.reset()

        if self.mySonar == None:
            self.deactivate()
            return None
        # get the range of the sonar
        # GLOBAL CONSTANTS FOR DEGREE START STOP USED BY SONAR
        range = self.mySonar.currentReadingPolar(-70, 70) - self.getRobot().getRobotRadius()
        time.sleep(.1)
        range_mod_right = self.mySonar.currentReadingPolar(-120, -70)
        time.sleep(.1)
        range_mod_left = self.mySonar.currentReadingPolar(70, 120)

        # if sonar range value is greater than the stop [STANDOFF] distance
        if range > self.myStopDistance:
            # modify speed since robot closing [STANDOFF] dist
            curr_speed = range * .3  # RENAME ARB_SPEED GLOBAL USE
            # if that speed is greater than our max, cap it
            if curr_speed > self.myMaxSpeed:
                curr_speed = self.myMaxSpeed
            # now set the velocity
            self.myDesired.setVel(curr_speed)
        else:
            # the range was less??GREATER[encroached standoff dist] than the stop distance, so request stop
            self.myDesired.setVel(0)
        # return reference notifies actionDesired resolver of request
        return self.myDesired

# Action turns robot away from sonar detected obstacles
class ActionTurn(ArAction):
    def __init__(self, turnThreshold, turnAmount):
        ArAction.__init__(self, "Turn")
        self.myDesired = ArActionDesired()
        self.myTurnThreshold = turnThreshold
        self.myTurnAmount = turnAmount
        self.myTurning = 0  # -1 == left, 1 == right, 0 == none

    # Override setRobot() gets reference to sonar device
    def setRobot(self, robot):
        """required override Parent class ArAction"""
        print "ActionTurn: calling ArAction.setActionRobot..."
        self.setActionRobot(robot)
        self.mySonar = robot.findRangeDevice("sonar")
        if self.mySonar == None:
            ArLog.log(ArLog.Terse, "ActionTurn: Unable to detect sonar range device, deactivating!")
            self.deactivate()

    #Turn Logic method fire() sensor data derived from onboard sonar range device
    def fire(self, currentDesired):
        """overrides ActionGo attr myDesired, to clear ->reset()
        required to clear previous values"""
        self.myDesired.reset()
        if self.mySonar == None:
            self.deactivate()
            return None
        # Get the left readings and right readings off of the sonar
        leftRange = (self.mySonar.currentReadingPolar(0, 100) -
                     self.getRobot().getRobotRadius())
        rightRange = (self.mySonar.currentReadingPolar(-100, 0) -
                      self.getRobot().getRobotRadius())
        # if neither left nor right range is within the turn threshold,
        # reset the turning variable and don't turn
        if (leftRange > self.myTurnThreshold and rightRange > self.myTurnThreshold):
            self.myTurning = 0
            self.myDesired.setDeltaHeading(0)
        # if we're already turning some direction, keep turning that direction
        elif (self.myTurning != 0):
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)
        # if we're not turning already, but need to, and leftRange is closer, turn right
        # and set the turning variable so we turn the same direction for as long as
        # we need to
        elif (leftRange < rightRange):
            self.myTurning = -1
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)
        # if we're not turning already, but need to, and rightRange is closer, turn left
        # and set the turning variable so we turn the same direction for as long as
        # we need to
        else:
            self.myTurning = 1
            self.myDesired.setDeltaHeading(self.myTurnAmount * self.myTurning)
        # return the actionDesired, so resolver knows what to do
        return self.myDesired

# class for navigation by dead reconing. Works however
# MagneticCompass.py is causing errors and needs to be debugged
class ActionDeadRecon(ArAction):
    """ constructor, sets myDesired"""
    def __init__(self):
        ArAction.__init__(self, "DR")
        self.myDesired = ArActionDesired()

    def setRobot(self, robot):
        """required override Parent class ArAction"""
        print "ActionDR: calling ArAction.setActionRobot..."
        self.setActionRobot(robot)

    #Navigation Logic method fire() implements MagneticCompass class
    def fire(self, currentDesired):
        """overrides ActionGo attr myDesired, to clear ->reset()
        required to clear previous values"""
        self.myDesired.reset()
        compassdata = compass.getcompassdata()
        # print "Compass Reading", compassdata
        trueheading = compass.trueheading(compassdata)
        # print "True Heading", trueheading
        # desiredheading = float(100.0)  # will need to be updated to get information from GPS at a later time
        current_location = my_GPS_object.getLatLon()  # current location is tuple in Decimal Degrees Lat Lon
        if current_location[0] == '' or current_location[1] == '':
            print "getLatLon returned empty Lat Lon, aborting this call to fire()"
            return
        target_location = navigation.get_current_waypoint()
        desiredheading = navigation.bearing_between_2_points_decimal_degrees(current_location[0], current_location[1],
                                                                             target_location[0], target_location[1])
        print "current location:", current_location, "\t target location:", target_location
        # If we're within
        delta_distance = navigation.distance_between_2_points_decimal_degrees(current_location[0], current_location[1], \
                                                                              target_location[0], target_location[1])
        if delta_distance < WAYPOINT_RADIUS:
            print "Reached Waypoint #", navigation.current_waypoint, " -- heading to next waypoint"
            navigation.current_waypoint += 1
            if navigation.current_waypoint >= len(navigation.waypoint_list):
                print "Mission Complete.  Quitting"
                quit()
            else:
                # just abort this call to fire() and recalculate bearing to new waypoint on next fire() call
                return
        else:
            print "Distance to waypoint", navigation.current_waypoint, "=", delta_distance
        """ MOVE TO PID CLASS DOC
        send data from sensor devices: -> : obj types: MagneticCompass, GPS
        compass ---> param: current->:value trueheading :type float
        GPS ---> param: desired ->:value desiredheading :type float gps data
        to PID.calculate_signal for turn calculation
        :returns self.myDesired :type ArActionDesired obj for resolver
        """
        input_err_sgnl = my_PID_object.calculate_PID_signal(desired=desiredheading, current=trueheading)
        #return PID plant evaluated heading change [degrees]
        hdgChange = my_PID_object.plant(input_err_sgnl)
        #String joining of method variables for formatted print
        data_str = 'Current Heading: {}\t Desired Heading: {}\t PID Calculation: {}\t Plant Heading Change:{}\t'.format(
            trueheading, desiredheading, input_err_sgnl, hdgChange)
        print(data_str)
        # self.myTurning = math.copysign(1,hdgChange)
        # hdgChange= 15.0
        self.myDesired.setDeltaHeading(hdgChange)

        return self.myDesired

class ActionAvoid(ArAction):
    """ constructor, sets myMaxSpeed and myStopDistance"""
    def __init__(self, maxSpeed, stopDistance):
        ArAction.__init__(self, "Go")
        self.myMaxSpeed = maxSpeed
        self.myStopDistance = stopDistance
        self.myDesired = ArActionDesired()
        self.mySonar = None

    # Override setRobot() gets reference to sonar device
    def setRobot(self, robot):
        """required override Parent class ArAction"""
        print "ActionAvoid: setting robot on ArAction..."
        self.setActionRobot(robot)
        # Find sonar device for use in fire():
        self.mySonar = robot.findRangeDevice("sonar")
        if self.mySonar == None:
            ArLog.log(ArLog.Terse,
                      "ActionGo: Warning: Unable to detect sonar range device, deactivating!")
            self.deactivate()

    # Avoidance Logic method fire()
    def fire(self, currentDesired):
        """overrides ActionGo attr myDesired, to clear ->reset()
        required to clear previous values"""
        self.myDesired.reset()

        if self.mySonar == None:
            self.deactivate()
            return None
        # get the range of the sonar
        # GLOBAL CONSTANTS FOR DEGREE START STOP USED BY SONAR
        range = self.mySonar.currentReadingPolar(-70, 70) - self.getRobot().getRobotRadius()
        time.sleep(.1)
        range_mod_right = self.mySonar.currentReadingPolar(-120, -70)
        time.sleep(.1)
        range_mod_left = self.mySonar.currentReadingPolar(70, 120)

        # if sonar range value is greater than the stop [STANDOFF] distance
        if range > self.myStopDistance:
            # modify speed since robot closing [STANDOFF] dist
            curr_speed = range * .3  # RENAME ARB_SPEED GLOBAL USE
            # if that speed is greater than our max, cap it
            if curr_speed > self.myMaxSpeed:
                curr_speed = self.myMaxSpeed
            # now set the velocity
            self.myDesired.setVel(curr_speed)
        else:
            # the range was less??GREATER[encroached standoff dist] than the stop distance, so request stop
            self.myDesired.setVel(0)
        # return reference notifies actionDesired resolver of request
        return self.myDesired




Aria_init()
parser = ArArgumentParser(sys.argv)
parser.loadDefaultArguments()
robot = ArRobot()
conn = ArRobotConnector(parser, robot)
sonar = ArSonarDevice()
# Create instances of the actions defined above, plus ArActionStallRecover,
# a predefined action from Aria_
go = ActionGo(500, 50)
turn = ActionTurn(400, turnAmount=10)
recover = ArActionStallRecover()
dr = ActionDeadRecon()
avoid = ActionAvoid(500, 50)

# Connect to the robot
if not conn.connectRobot():
    ArLog.log(ArLog.Terse, "actionExample: Unable to connect to robot.  Exiting...")
    Aria_exit(1)

# Parse all command-line arguments
if not Aria_parseArgs():
    Aria_logOptions()
    Aria_exit(1)
# Add the range device to the robot before adding any actions
robot.addRangeDevice(sonar)
# Add our actions in order. The second argument is the priority,
# with higher priority actions going first, and possibly pre-empting lower
# priority actions.

#Add Actions to robot after registering range devices -> robot.addRangeDevice(devicename=sonar)
def addActions(self):
    """Add user defined actions in sequence order (i.e Desired Task priority and associated priority value)
    :argument name of ArAction subclass instance (e.g. go=ActionGo(ArAction)...
    :argument priority :type int [between 0-99]?
    note: higher valued priorities may possibly pre-empting lower and unexpected behavior"""

    # robot.addAction(recover, 100)
    robot.addAction(go, 61)
    # robot.addAction(turn, 50)
    robot.addAction(dr, 60)
    robot.addAction(avoid, 59)
    ##TODO validate no side-effects from commented addActions -> turn, recover


# Enable the motors
robot.enableMotors()
# Run the robot processing cycle.
# 'true' means to return if it loses connection,
# after which we exit the program.
robot.run(1)
##TODO prompt if user wants to run teleop.mode
while(1):
    usrchoice = raw_input("Press any key to run Teleop mode. Otherwise press q to quit \n")
    if usrchoice =="q":
        print('Exiting. Please wait')
        time.sleep(2)
        exit()
    else:
        try:
            #mode = subprocess.Popen([sys.executable,"Teleop.py"]) #windows
            subprocess.call(['python','teleop.py'])
            os.chdir('/Users/student/')
            os.environ.get('HOME')
            inFile = 'teleop.py'
            file_path = os.path.join(os.environ.get('HOME'),inFile)
            if os.path.exists(file_path)==True:
                subprocess.call('./teleop.py')
            #Aria\pythonExamples
        except Exception as e:
            print str(e)
        else:
            print"Error in attempt to run File: teleop.py"

Aria_exit(0)
